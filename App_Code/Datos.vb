﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration

Public Class Datos
    Public Shared cnn As String = ConfigurationManager.AppSettings.Get("cnn")

    Public Shared Function ejecutarSP(ByVal sentencia As String, ByRef ErrMensaje As String, parametros() As SqlParameter) As Boolean
        Dim con As New SqlConnection
        Dim comando As New SqlCommand
        Try
            con.ConnectionString = cnn
            con.Open()
            comando.Connection = con
            comando.CommandType = CommandType.StoredProcedure
            If Not IsNothing(parametros) Then
                For cont As Integer = 0 To parametros.Length - 1
                    comando.Parameters.Add(parametros(cont))
                Next
            End If
            comando.CommandText = sentencia
            comando.ExecuteNonQuery()
        Catch ex As Exception
            ErrMensaje = ex.Message
            Return False
        End Try

        Return True
    End Function

    Public Shared Function ejecutarSPDataSet(ByVal sentencia As String, ByVal parametros() As SqlParameter, ByRef ErrMensaje As String) As DataSet
        Dim con As New SqlConnection
        Try

            Dim comando As New SqlCommand
            Dim adap As New SqlDataAdapter
            Dim dset As New DataSet
            con.ConnectionString = cnn
            con.Open()
            comando.CommandTimeout = 0
            comando.Connection = con
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = sentencia

            If Not IsNothing(parametros) Then
                For cont As Integer = 0 To parametros.Length - 1
                    comando.Parameters.Add(parametros(cont))
                Next
            End If

            adap.SelectCommand = comando
            adap.Fill(dset)
            ErrMensaje = ""
            Return dset
        Catch ex As Exception
            Console.WriteLine(ex.Message.Trim)
            ErrMensaje = ex.Message.Trim
        Finally
            con.Close()
        End Try

    End Function

    Public Shared Function EjecutarDML(ByVal sentencia As String, ByRef ErrMensaje As String) As Boolean
        Dim comando As New SqlCommand
        Dim con As New SqlConnection
        Try
            con.ConnectionString = cnn
            con.Open()
            comando.Connection = con
            comando.CommandType = CommandType.Text
            comando.CommandText = sentencia
            comando.ExecuteNonQuery()
            con.Close()
        Catch ex As Exception
            ErrMensaje = ex.Message
            Return False
        End Try

        Return True

    End Function



    Public Shared Function ejecutarQuery(ByVal sentencia As String) As Data.DataSet
        Dim con As New SqlConnection

        Try
            Dim comando As New SqlCommand
            Dim adap As New SqlDataAdapter
            Dim dset As New DataSet
            con.ConnectionString = cnn
            con.Open()
            comando.CommandTimeout = 0
            comando.Connection = con
            comando.CommandType = CommandType.Text
            comando.CommandText = sentencia
            adap.SelectCommand = comando
            adap.Fill(dset)
            Return dset
        Catch ex As Exception
            Console.WriteLine(ex.Message.Trim)
            'ErrMensaje = ex.Message.Trim
        Finally
            con.Close()
        End Try
    End Function

    Public Shared Function ejecutarQuery(ByVal sentencia As String, ByRef ErrMensaje As String) As Data.DataSet
        Dim con As New SqlConnection

        Try
            Dim comando As New SqlCommand
            Dim adap As New SqlDataAdapter
            Dim dset As New DataSet
            con.ConnectionString = cnn
            con.Open()
            comando.CommandTimeout = 0
            comando.Connection = con
            comando.CommandType = CommandType.Text
            comando.CommandText = sentencia
            adap.SelectCommand = comando
            adap.Fill(dset)
            Return dset
        Catch ex As Exception
            ErrMensaje = ex.Message.Trim
        Finally
            con.Close()
        End Try
    End Function



End Class
