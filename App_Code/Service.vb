﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Configuration.ConfigurationManager
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports System.Drawing
Imports System.Drawing.Imaging
Imports Zen.Barcode
Imports System.IO


' Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente.
' <System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://tempuri.org/")>
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Public Class Service
    Inherits System.Web.Services.WebService
    Private _Cadena As String
    Private _FolioCadena As String
    Private _Sucursal As String
    Private _Correo As String = ""
    Private _Vigencia As String = ""
    Private _Desc As String = ""
    Private _Prom As String = ""
    Private _Celular As String = ""
    Private _ConexionTienda As String
    Private bdErr As String = ""
    Private dt As New DataTable
    Private MensajeDummy As String = ""
    Private MensajeReturn As String = ""
    Private _FolioNuevo As String
    Private _CodBarrasImagen() As Byte
    Private nombreTienda As String
    Public authModel As New AuthModel
    Dim b As New BitacoraError

    <WebMethod(), SoapHeader("authModel")>
    Public Function CLUB_BB(Cadena As String, Promocion As String, Correo As String, Celular As String) As CuponBB
		Celular = IIf(Celular = "0","",Celular)
		Correo = IIf(Correo = "0","",Correo)
		
        b.RevisarBitacora()
        b.AgregarLinea(Cadena)
        Me._Celular = Celular
        Me._Correo = Correo
        If Not (authModel._Token = "") Then
            If isValid(authModel) Then
                _Cadena = Cadena
                If Not GetPromocionID(Promocion) Then
                    b.AgregarLinea("MSG00")
                    MensajeReturn = "Folio Invalido- No se detecta folio. "
                End If
                If Not GetSucursal() Then
                    b.AgregarLinea("MSG00")
                    MensajeReturn = "Folio Invalido- No se detecta sucursal en folio. "
                    Return SendBadRequest("MSG00", MensajeReturn)
                End If
                If Not GetConexionTienda() Then
                    b.AgregarLinea("MSG00")
                    MensajeReturn = "Folio Invalido- No se detecta sucursal en folio "
                    Return SendBadRequest("MSG00", MensajeReturn)
                End If
                If ValidaExisteFolioYCorrespondePromocion(MensajeDummy, MensajeReturn, _Prom) Then
                    Dim tiendaID = _Cadena.Substring(0, 3)
                    Dim especializadas = ConfigurationManager.AppSettings.Get("especializadas").Split(",")
                    If tiendaID.Substring(0, 1) = "1" Then
                        Me.nombreTienda = "ds"
                    ElseIf tiendaID.Substring(0, 1) = "2" Then
                        Me.nombreTienda = "ww"
                    ElseIf especializadas.Length > 0 Then
                        Me.nombreTienda = "ds"
                        For i As Integer = 0 To especializadas.Length - 1
                            If especializadas(i) = tiendaID Then
                                Me.nombreTienda = "ww"
                            End If
                        Next
                    Else
                        Me.nombreTienda = "ds"
                    End If
                    If ValidaNoRegistrado() Then
                        If ObtieneNuevoFolio(MensajeReturn, MensajeDummy) Then
                            If GuardaTransaccionOmnitrans() Then
                                If GeneraCodBarras() Then
                                    If GuardaCentralBitacora() Then
                                        b.AgregarLinea("03")
                                        Return SendOKRequest()
                                    Else
                                        b.AgregarLinea("MSG09 GuardarBitacoraCentral")
                                        MensajeReturn = "Envió No Satisfactorio - Error al enviar a Landin "
                                        Return SendBadRequest("MSG09", MensajeReturn)
                                    End If
                                Else
                                    b.AgregarLinea("MSG09 GenerarCodBarras")
                                    MensajeReturn = "Envió No Satisfactorio - Error al enviar a Landin "
                                    Return SendBadRequest("MSG09", MensajeReturn)
                                End If
                            Else
                                b.AgregarLinea("MSG07")
                                MensajeReturn = "Error Insertar Cupón - Error al insertar cupón en omnitrans "
                                Return SendBadRequest("MSG07", MensajeReturn)
                            End If
                        Else

                            Return SendBadRequest(MensajeDummy, MensajeReturn)
                        End If
                    Else
                        'El Folio ya registrado.
                        MensajeReturn = "Folio Ya Registrado - Ya se encuentra en tabla centralizada "
                        b.AgregarLinea("MSG04")
                        Return SendBadRequest("MSG04", MensajeReturn)
                    End If
                Else
                    b.AgregarLinea(MensajeDummy)
                    Return SendBadRequest(MensajeDummy, MensajeReturn)
                End If
            Else
                b.AgregarLinea("Auth01")
                'Dim dt2 As New DataTable("tbl1")
                Return SendBadRequest("Auth01", "No se encontro un acceso con estos datos")

            End If
        Else
            b.AgregarLinea("Auth00")
            Return SendBadRequest("Auth00", "No se especificaron datos de authenticacion")
        End If
    End Function

    Private Function GeneraCodBarras() As Boolean
        Try
            Dim ms As New MemoryStream
            Dim bm As New Bitmap(300, 500, PixelFormat.Format24bppRgb)
            Dim g As Graphics = Graphics.FromImage(bm)
            g.Clear(Color.White)
            GENERARBMP(g, _FolioNuevo)
            ' bm.Save("prueba-1.bmp")

            bm.Save(ms, ImageFormat.Png)
            _CodBarrasImagen = ms.ToArray()
            Return True

        Catch ex As Exception
            b.AgregarLinea("GeneraCodBarras: " & ex.Message)
            Return False
        End Try
    End Function

    Private Sub GENERARBMP(e As Graphics, folioNuevo As String)
        Try



            Dim textlocation As Point
            Dim imgeLocation As Point
            Dim newImage As Image
            Dim a As New SolidBrush(ColorTranslator.FromHtml("#2b549f"))
            Dim b As New SolidBrush(ColorTranslator.FromHtml("#31b3b4"))


            textlocation = New Point(5, 240)
            e.DrawString("*El descuento aplica a todas tus compras realizadas en", New Font("Arial", 8, FontStyle.Bold), a, textlocation)
            textlocation = New Point(2.5, 260)
            e.DrawString("2019 en todos los departamentos de ropa, accesorios,", New Font("Arial", 8, FontStyle.Bold), a, textlocation)


            textlocation = New Point(70, 280)
            e.DrawString("blancos y perfumerí­a para bebé.", New Font("Arial", 8, FontStyle.Bold), a, textlocation)

            textlocation = New Point(30, 300)
            e.DrawString("(no aplica en pañales ni alimentos para bebé)", New Font("Arial", 8, FontStyle.Bold), a, textlocation)

            Dim drawcode As Code128BarcodeDraw = BarcodeDrawFactory.Code128WithChecksum
            newImage = drawcode.Draw(folioNuevo, 50)

            imgeLocation = New Point(85, 340)
            e.DrawImage(newImage, imgeLocation)
            'Texto del folio
            textlocation = New Point(105, 390)
            e.DrawString(folioNuevo, New Font("Arial", 8, FontStyle.Bold), Brushes.Black, textlocation)

            textlocation = New Point(10, 420)
            e.DrawString("Válido hasta el", New Font("Arial", 12, FontStyle.Bold), a, textlocation)
            textlocation = New Point(125, 420)
            e.DrawString("31 de diciembre 2019", New Font("Arial", 12, FontStyle.Bold), b, textlocation)
            textlocation = New Point(20, 440)
            e.DrawString("Presentando este cupón en caja", New Font("Arial", 12, FontStyle.Bold), a, textlocation)

            'folio


            imgeLocation = New Point(0, 0)
            If Me.nombreTienda = "ds" Then
                newImage = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory & "encabezado_peDS.png")
            ElseIf Me.nombreTienda = "ww" Then
                newImage = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory & "encabezado_pe.png")
            Else
                newImage = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory & "encabezado_peDS.png")
            End If
            e.DrawImage(newImage, imgeLocation)

        Catch ex As Exception
            b.AgregarLinea("GeneraBMP: " & ex.Message)
        End Try
    End Sub

    Private Function GuardaCentralBitacora() As Boolean
        Try
            Datos.cnn = AppSettings.Get("CnnCentralClubBB")
            bdErr = ""
            Dim par(10) As SqlParameter
            par(0) = New SqlParameter("@Accion", 2)
            par(1) = New SqlParameter("@FolioCompra", _Cadena)
            par(2) = New SqlParameter("@NumeroCupon", _FolioCadena)
            par(3) = New SqlParameter("@Por_Desc", _Desc)
            par(4) = New SqlParameter("@Vigencia", _Vigencia)
            par(5) = New SqlParameter("@Imagen", _CodBarrasImagen)
            par(6) = New SqlParameter("@CodCupon", _FolioNuevo)
            par(7) = New SqlParameter("@Correo", _Correo)
            par(8) = New SqlParameter("@Estatus", 1)
            par(9) = New SqlParameter("@Id_Promo", _Prom)
            par(10) = New SqlParameter("@Celular", _Celular)
            If Datos.ejecutarSP(Constantes.proc_ValidaPromoClubBB, bdErr, par) Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception

            b.AgregarLinea("GuardaCentralBiracora: " & ex.Message)
            Return False
        End Try
    End Function

    Private Function GuardaTransaccionOmnitrans() As Boolean
        Try
            Datos.cnn = AppSettings.Get("CnndbCliente")
            bdErr = ""
            Dim par(1) As SqlParameter
            par(0) = New SqlParameter("@Cuenta", _FolioNuevo)
            par(1) = New SqlParameter("@Sucursal", _Sucursal)
            If Datos.ejecutarSP(Constantes.Proc_GuardaOmnitransClubBB, bdErr, par) Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            b.AgregarLinea("GuardaTransaccionOmnitrans " & ex.Message)
            Return False
        End Try
    End Function

    Private Function ObtieneNuevoFolio(ByRef MensajeReturn As String, ByRef Mensaje As String) As Boolean
        Try
            Datos.cnn = AppSettings.Get("CnndbCliente")
            bdErr = ""
            dt = New DataTable
            dt = Datos.ejecutarSPDataSet(Constantes.PROC_FOLIOSCLUBBEBE, Nothing, bdErr).Tables(0)
            If Not IsNothing(dt) Then
                _FolioNuevo = dt.Rows(0).Item(0).ToString
                If _FolioNuevo = "" Then
                    MensajeReturn = "Error Generación Cupón - Error al no encontrar cupones disponibles "
                    Mensaje = "MSG05"
                    Return False
                End If
                Return True
            Else
                MensajeReturn = "Error Generación Cupón - Error al no encontrar cupones disponibles "
                Mensaje = "MSG05"
                Return False
            End If
        Catch ex As Exception
            b.AgregarLinea("Conexión Fallida Omnitrans - No hay conexión a omnitrans " & ex.Message)
            MensajeReturn = "Conexión Fallida Omnitrans - No hay conexión a omnitrans"
            Mensaje = "MSG06"
            Return False
        End Try
    End Function

    Private Function ValidaNoRegistrado() As Boolean
        Try
            Datos.cnn = AppSettings.Get("CnnCentralClubBB")
            bdErr = ""
            Dim par(1) As SqlParameter
            par(0) = New SqlParameter("@Accion", 1)
            par(1) = New SqlParameter("@FolioCompra", _Cadena)
            dt = New DataTable
            dt = Datos.ejecutarSPDataSet(Constantes.proc_ValidaPromoClubBB, par, bdErr).Tables(0)
            If Not IsNothing(dt) Then
                If dt.Rows(0).Item(0) > "0" Then
                    Return False
                Else

                    Return True
                End If
            Else
                Return False
            End If
        Catch ex As Exception
            b.AgregarLinea("ValidaNoRegistrado " & ex.Message)
            Return False
        End Try
    End Function

    Private Function ValidaExisteFolioYCorrespondePromocion(ByRef Mensaje As String, ByRef MensajeReturn As String, ByVal Promocion As String) As Boolean
        Try
            _FolioCadena = _Cadena.Substring(13, 13)
            Datos.cnn = _ConexionTienda
            bdErr = ""
            Dim par(3) As SqlParameter
            par(0) = New SqlParameter("@Folio", _FolioCadena)
            par(1) = New SqlParameter("@Caja", _Cadena.Substring(6, 3))
            par(2) = New SqlParameter("@Cajero", _Cadena.Substring(3, 3))
            par(3) = New SqlParameter("@Transaccion", _Cadena.Substring(9, 4))
            dt = New DataTable
            dt = Datos.ejecutarSPDataSet(Constantes.SP_PODBDAT, par, bdErr).Tables(0)
            If Not IsNothing(dt) And dt.Rows.Count > 0 Then
                If dt.Rows(0).Item(0) = Promocion Then
                    Return True
                Else
                    'Folio no valido para esta promocion
                    MensajeReturn = "Folio no valido de club bebe - no valido de promocion club bebe."
                    Mensaje = "MSG01"
                    Return False
                End If
            Else
                b.AgregarLinea("No se encontro el folio")
                MensajeReturn = "No Se Encuentra Folio - No encontró Folio en tabla de sucursal ValidaExisteFolioYCorrespondePromocion "
                Mensaje = "MSG02"
                Return False
            End If
        Catch ex As Exception
            'Genere la consulta un momento más tarde.
            b.AgregarLinea("ValidaExisteFolioYCorrespondePromocion " & ex.Message)
            MensajeReturn = "Conexión Sucursal Fallida - Conexión a tienda fallida"
            Mensaje = "MSG03"
            Return False
        End Try
    End Function

    Private Function GetConexionTienda() As Boolean
        Try
            Datos.cnn = AppSettings.Get("CnnCentralClubBB")
            dt = New DataTable
            Dim p(0) As SqlParameter
            p(0) = New SqlParameter("@NoTienda", _Sucursal)
            dt = New DataTable
            dt = Datos.ejecutarSPDataSet(Constantes.Sp_ConexionesTiendas, p, bdErr).Tables(0)
            'Data source=10.0.1.156;user id=sa;password=gcc11#pws1;initial catalog=FacturacionWebP;Trusted_Connection=False
            If dt.Rows.Count > 0 Then
                With dt.Rows(0)
                    '_ConexionTienda = String.Format("Data source={0};user id={1};password={2};initial Catalog=Posbdat;Trusted_Connection=False", .Item("IP"), .Item("UserID"), .Item("Password"))
                    _ConexionTienda = String.Format("Data source={0},{1};user id={2};password={3};initial Catalog=Posbdat;Trusted_Connection=False", .Item("IP"), .Item("Puerto"), .Item("UserID"), .Item("Password"))
                    Return True
                End With
            End If
        Catch ex As Exception
            b.AgregarLinea("GetConexionTienda " & ex.Message)
            _ConexionTienda = ""
            Return False
        End Try
    End Function

    Private Function GetPromocionID(Promocion As String) As Boolean
        Try
            Datos.cnn = AppSettings.Get("CnnCentralClubBB")
            dt = New DataTable
            Dim p(0) As SqlParameter
            p(0) = New SqlParameter("@promo", Promocion)
            dt = New DataTable
            dt = Datos.ejecutarSPDataSet("GetPromocion", p, bdErr).Tables(0)
            'Data source=10.0.1.156;user id=sa;password=gcc11#pws1;initial catalog=FacturacionWebP;Trusted_Connection=False
            If dt.Rows.Count > 0 Then
                With dt.Rows(0)
                    Me._Prom = .Item("Id_Promo")
                    Me._Desc = .Item("Por_Desc")
                    Me._Vigencia = .Item("Vigencia")
                    Return True
                End With
            End If
        Catch ex As Exception
            b.AgregarLinea("GetPromocion  " & ex.Message)
            _ConexionTienda = ""
            Return False
        End Try
    End Function

    Public Function SendBadRequest(message As String, errorMsg As String) As CuponBB
        Dim Response As New CuponBB
        Response.Imagen = ""
        Response.Mensaje = message
        Response.NumCupon = ""
        Return Response
    End Function

    Public Function SendOKRequest() As CuponBB
        Dim Response As New CuponBB
        Response.Imagen = Convert.ToBase64String(_CodBarrasImagen.ToArray())
        Response.Mensaje = "03"
        Response.NumCupon = _FolioNuevo
        Return Response
    End Function

    Private Function GetSucursal() As Boolean
        Try
            _Sucursal = _Cadena.Substring(0, 3)
            Return True
        Catch ex As Exception
            b.AgregarLinea("GetSucursal " & ex.Message)
            Return False
        End Try
    End Function
    Private Function isValid(auth As AuthModel) As Boolean
        Try
            Dim b As Byte() = Convert.FromBase64String(auth._Token)
            Dim str As String = System.Text.Encoding.UTF8.GetString(b)
            'Datos.cnn = ConfigurationManager.AppSettings.Get("cnnSeguridad")
            Dim para(1) As SqlParameter
            para(0) = New SqlParameter("@TOKEN", str)
            para(1) = New SqlParameter("@Accion", 3)
            Dim MEN As String = ""
            Dim ds As New DataSet
            ds = ejecutarSPDataSet("sp_AdminToken", para, MEN)
            If MEN = "" Then
                If ds.Tables(0).Rows(0).Item(0) = "OK" Then
                    Return True
                Else
                    Return False
                End If
            Else
                Return False
            End If
        Catch ex As Exception
            b.AgregarLinea("isValid " & ex.Message)
            Return False
        End Try
    End Function

    Private cnn As String = ConfigurationManager.AppSettings.Get("cnnSeguridad")
    Private Function ejecutarSPDataSet(ByVal sentencia As String, ByVal parametros() As SqlParameter, ByRef ErrMensaje As String) As DataSet
        Dim con As New SqlConnection
        Try

            Dim comando As New SqlCommand
            Dim adap As New SqlDataAdapter
            Dim dset As New DataSet
            con.ConnectionString = cnn
            con.Open()
            comando.CommandTimeout = 0
            comando.Connection = con
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = sentencia
            For cont As Integer = 0 To parametros.Length - 1
                comando.Parameters.Add(parametros(cont))
            Next
            adap.SelectCommand = comando
            adap.Fill(dset)
            ErrMensaje = ""
            Return dset
        Catch ex As Exception
            b.AgregarLinea("ejecutarSPDataSet: " & ex.Message)
            Console.WriteLine(ex.Message.Trim)
            ErrMensaje = ex.Message.Trim
        Finally
            con.Close()
        End Try

    End Function
    <WebMethod()>
    Public Function Authenticate(User As String, Password As String) As String
        Try
            Dim b As Byte() = Convert.FromBase64String(User)
            Dim strUser As String = System.Text.Encoding.UTF8.GetString(b)

            Dim bb As Byte() = Convert.FromBase64String(Password)
            Dim strPass As String = System.Text.Encoding.UTF8.GetString(bb)
            b = Nothing
            bb = Nothing
            cnn = ConfigurationManager.AppSettings.Get("cnnSeguridad")
            Dim para(3) As SqlParameter
            para(0) = New SqlParameter("@USER", strUser)
            para(1) = New SqlParameter("@PASS", strPass)
            para(2) = New SqlParameter("@IdSistema", ConfigurationManager.AppSettings.Get("Sis"))
            para(3) = New SqlParameter("@Accion", 2)
            Dim MEN As String = ""
            Dim ds As New DataSet
            ds = ejecutarSPDataSet("sp_AdminToken", para, MEN)
            If MEN = "" Then
                Return ds.Tables(0).Rows(0).Item(0).ToString
            Else
                Return MEN
            End If
        Catch ex As Exception
            b.AgregarLinea("Authenticate" & ex.Message)
        End Try
    End Function
End Class








Public Class Constantes
    Public Shared ReadOnly Sp_ConexionesTiendas As String = "sp_ConexionTiendas_ClubBB"
    Public Shared ReadOnly SP_PODBDAT As String = "SP_PromoClubBB"
    Public Shared ReadOnly proc_ValidaPromoClubBB As String = "proc_ValidaPromoClubBB"
    Public Shared ReadOnly PROC_FOLIOSCLUBBEBE As String = "PROC_FOLIOSCLUBBEBE"
    Public Shared ReadOnly Proc_GuardaOmnitransClubBB As String = "Proc_GuardaOmnitransClubBB"

End Class


Public Class CuponBB
    Private _mensaje As String = ""
    Private _imagen As String = ""
    Private _numCupon As String = ""

    Public Property Mensaje As String
        Get
            Return _mensaje
        End Get
        Set(value As String)
            _mensaje = value
        End Set
    End Property



    Public Property Imagen As String
        Get
            Return _imagen
        End Get
        Set(value As String)
            _imagen = value
        End Set
    End Property

    Public Property NumCupon As String
        Get
            Return _numCupon
        End Get
        Set(value As String)
            _numCupon = value
        End Set
    End Property
End Class

Public Class AuthModel : Inherits System.Web.Services.Protocols.SoapHeader
    Public _Token As String
End Class

