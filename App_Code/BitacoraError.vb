﻿Imports System.IO
'Imports System.IO.FileInfo

Public Class BitacoraError
    Private Ruta As String = System.AppDomain.CurrentDomain.BaseDirectory() & "Bitacora\" & Today.Date.ToString("ddMMyyyy", New System.Globalization.CultureInfo("es-ES")) & "\"
    Private Ruta2 As String = "BitacoraError " & Now.ToString("yyyyMMddHHmmss") & ".txt"
    Private RutaBitacora As String = Ruta & Ruta2

    Public Sub AgregarLinea(ByVal Mensaje As String)
        Dim W As StreamWriter = File.AppendText(RutaBitacora)
        W.WriteLine(Mensaje)
        W.Close()
    End Sub
    'If FileLen(RutaBitacora) >= 122880 Then 'en bits - 120 kb
    'File.Move(RutaBitacora, System.AppDomain.CurrentDomain.BaseDirectory() & Format(Now, "ddMMyyyy_hhmmss") & "BitacoraTransferenciaArchivos.txt")
    'End If
    Public Sub RevisarBitacora()
        If File.Exists(RutaBitacora) Then 'borrar el que esta de ese día
            Dim dir As New DirectoryInfo(Ruta)
            Dim oFile As FileInfo
            For Each oFile In dir.GetFiles
                If oFile.Name.Substring(0, 22) = Ruta2.Substring(0, 22) Then
                    If FileLen(RutaBitacora) >= 122880 Then
                        File.Delete(Ruta & oFile.Name)
                    End If
                    Exit For
                End If
            Next
        Else
            If Not Directory.Exists(Ruta) Then
                Directory.CreateDirectory(Ruta)
            End If
        End If
    End Sub
End Class
